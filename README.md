# Hands-On Lab Exercises

This project has the Markdown step-by-step instructions and screenshots for all of our hands-on lab exercises that are used for demos, training classes, tutorials, and workshops.

## Continuous Integration (CI)

* [CI Pipeline Basics](v13/ci-pipeline-basics/exercise.md)
